package com.gox.base.views.paypal

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProviders
import com.gox.base.R
import com.gox.base.base.BaseActivity
import com.gox.base.databinding.ActivityPaypalBinding
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.nio.charset.StandardCharsets


class PayPalPayment : BaseActivity<ActivityPaypalBinding>() {
    private lateinit var activityPaypalBinding: ActivityPaypalBinding
    private lateinit var payPalViewModel: PayPalViewModel
    private var payPalurl: String = ""

    init {
        loadingObservable.value=true
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_paypal
    }

    override fun initView(mViewDataBinding: ViewDataBinding?) {
        activityPaypalBinding = mViewDataBinding as ActivityPaypalBinding
        payPalViewModel = ViewModelProviders.of(this).get(PayPalViewModel::class.java)
        activityPaypalBinding.paypalViewModel = payPalViewModel
        getIntentValues()
        initWebView()
    }

    fun getIntentValues() {
        payPalurl = if (intent != null && intent.hasExtra("payPalurl")) intent.getStringExtra("payPalurl") else ""
    }


    fun initWebView() {
        activityPaypalBinding.wvPayment.settings.javaScriptEnabled = true
        activityPaypalBinding.wvPayment.setWebChromeClient(WebChromeClient())
        activityPaypalBinding.wvPayment.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        activityPaypalBinding.wvPayment.webViewClient = WebClient()
        activityPaypalBinding.wvPayment.loadUrl(payPalurl)
    }


    inner class WebClient : WebViewClient() {
        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            loadingObservable.value=false
            if (url!!.contains("success")) {
                Log.e("Url", "-------- finished" + url)
            }
        }

        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            val uri: Uri = Uri.parse(url)
            val paramNames: Set<String> = uri.getQueryParameterNames()
            val paymentHash=HashMap<String,String>()
            for (key in paramNames) {
                val value: String = uri.getQueryParameter(key)
                Log.d("PayPalKey", "shouldOverrideURLLoading: name: $key value: $value")
                when (key) {
                    "order" -> paymentHash.put("order",value)

                        "status" -> paymentHash.put("status",value)

                        "paymentId" -> paymentHash.put("paymentId",value)

                        "token" -> paymentHash.put("token",value)

                        "PayerID" -> paymentHash.put("PayerID",value)
                }
            }
            if(!paymentHash.isEmpty() && paymentHash.containsKey("paymentId")) {
                intent.putExtra("paypal", paymentHash)
                setResult(211, intent)
                finish()
            }
            url?.let { view?.loadUrl(it) }
            return true
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            Log.e("PayPal", "------" + url)
            if (url!!.contains("transaction_status=capture")) {

            }
            super.onPageStarted(view, url, favicon)
           // loadingObservable.value=true
        }

    }


    @Throws(IOException::class)
    private fun convertInputStreamToString(inputStream: InputStream): String? {
        val result = ByteArrayOutputStream()
        val buffer = ByteArray(1024)
        var length: Int? = 0
        while (inputStream.read(buffer).also({ length = it }) != -1) {
            result.write(buffer, 0, length!!)
        }
        return result.toString(StandardCharsets.UTF_8.name())
    }

}