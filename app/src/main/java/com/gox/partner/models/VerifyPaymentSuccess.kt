package com.gox.partner.models


data class VerifyPaymentSuccess(
        val error: List<Any?>? = listOf(),
        val message: String? = "",
        val responseData: ResponseData? = ResponseData(),
        val statusCode: String? = "",
        val title: String? = ""
) {
    data class ResponseData(
            val admin_service: String? = "",
            val amount: Int? = 0,
            val company_id: Int? = 0,
            val created_at: String? = "",
            val created_by: Int? = 0,
            val created_type: String? = "",
            val deleted_by: Any? = Any(),
            val deleted_type: Any? = Any(),
            val id: Int? = 0,
            val is_wallet: Int? = 0,
            val modified_by: Int? = 0,
            val modified_type: String? = "",
            val order_request: Any? = Any(),
            val payment_id: String? = "",
            val payment_mode: String? = "",
            val response: String? = "",
            val transaction_code: String? = "",
            val transaction_id: Any? = Any(),
            val updated_at: String? = "",
            val user_id: Int? = 0,
            val user_type: String? = ""
    )
}